# UIKit - UITableViewDelegate

```swift
// MARK: -
// MARK: Interface implementation protocol UITableViewDelegate
// MARK: -
extension <#ClassViewController#>: UITableViewDelegate {

    public func tableView(_ tableView: UITableView, heightForRowAt pIndexPath: IndexPath) -> CGFloat {
        return <#cell height#>
    }

    public func tableView(_ pTableView: UITableView, willDisplay cell: UITableViewCell, forRowAt pIndexPath: IndexPath) {
    }

    public func tableView(_ pTableView: UITableView, didSelectRowAt pIndexPath: IndexPath) {
    }

    public func tableView(_ tableView: UITableView, didDeselectRowAt pIndexPath: IndexPath) {
    }

}
```
