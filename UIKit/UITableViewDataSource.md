# UIKit - UITableViewDataSource

```swift
// MARK: -
// MARK: Interface implementation protocol UITableViewDataSource
// MARK: -
extension <#ClassViewController#>: UITableViewDataSource {

    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt pIndexPath: IndexPath) -> UITableViewCell {
        let lItem = items[pIndexPath.row]
        
        guard let lRet = tableView.dequeueReusableCell(withIdentifier: “<“#cel identifier#>, for: pIndexPath) as? HTAFormListCell else {
            return UITableViewCell()
        }
        
        lRet.lbl<#label#>.textColor = UIColor(named: “<#asset color#>”)
        lRet.lbl<#label#>.text = lItem.value

        lRet.backgroundColor = UIColor(named: “<#asset color#>”)
        
        return lRet
    }
}
```
