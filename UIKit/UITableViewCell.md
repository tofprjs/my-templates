# TUIKit - UITableViewCell

```swift
// MARK: -
// MARK: <#class#>Cell
// MARK: -
public class <#class#>Cell : UITableViewCell {
    // MARK: -
    // MARK: Public access
    // MARK: -
    
    // MARK: -> Public properties
    
    //@IBOutlet weak var lbl<#label name#>: UILabel!
    //@IBOutlet weak var img<#image name#>: UIImageView!
    //@IBOutlet weak var bt<#button name#>: UIButton!
    //@IBOutlet weak var view<#view name#>: UIView!
    //@IBOutlet weak var stack<#stack name#>: UIStack!
    
    // MARK: -> Public class methods
    
    // MARK: -> Public init methods
    
    // MARK: -> Public methods
}
```
