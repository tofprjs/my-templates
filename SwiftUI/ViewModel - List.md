# ViewModel - List

```swift
// MARK: -
// MARK: <#ViewModelList#>
// MARK: -
public class <#ViewModelList#>: ObservableObject {
  // MARK: -
  // MARK: Public access
  // MARK: -
  
  // MARK: -> Public enums
  
  // MARK: -> Public structs
  
  // MARK: -> Public class
  
  // MARK: -> Public type alias 

  // MARK: -> Public published properties

  @Published public var items:[<#ViewModelItem#>] = []

  // MARK: -> Public binding properties

  // MARK: -> Public static properties
  
  // MARK: -> Public properties
  
  // MARK: -> Public static methods
  
  // MARK: -> Public init methods
  
  // MARK: -> Public methods

  public func delete() {
      <#Model#>.delete() // With TStorage
  }

  public func fetch() {
      items = [] // TODO: call API to fetch items
  }

  // MARK: -
  // MARK: Private access
  // MARK: -
  
  // MARK: -> Private enums
  
  // MARK: -> Private structs
  
  // MARK: -> Private class
  
  // MARK: -> Private type alias 

  // MARK: -> Private static properties

  // MARK: -> Private properties

  // MARK: -> Private static methods
  
  // MARK: -> Private init methods
  
  // MARK: -> Private operators

  // MARK: -> Private methods

  // MARK: -
  // MARK: Dummy datas
  // MARK: -
    
  //public class Dummy {
  //      
  //      public static var <#one instance#>:<#ViewModel#> {
  //          return <#ViewModel#>()
  //      }
  //}
}
```