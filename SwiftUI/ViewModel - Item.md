# ViewModel - Item

```swift
// MARK: -
// MARK: <#ViewModelItem#>
// MARK: -
public struct <#ViewModelItem#> {
  // MARK: -
  // MARK: Public access
  // MARK: -
  
  // MARK: -> Public enums
  
  // MARK: -> Public structs
  
  // MARK: -> Public class
  
  // MARK: -> Public type alias 

  public typealias Model = <#model type#>

  // MARK: -> Public published properties

  // MARK: -> Public binding properties

  // MARK: -> Public static properties

  // MARK: -> Public properties

  public var <#property#>:<#type#> {
      model.<#property#> // orignal property or a formated property
  }

  // MARK: -> Public static methods
  
  // With TStorage
  public static func item(id pId:String) -> <#ViewModelItem#>? {
      let lSql = <#Model#>.sql
          .where(.eq("id",pId))
          .limit(1)

      var lRet:<#ViewModelItem#>? = nil
      
      if let lItem = <#Model#>.select(lSql).first {
          lRet = <#ViewModelItem#>(model: lItem)
      }

      return lRet
  }

  // MARK: -> Public init methods
  
  public init() {
      model = Model()
  }
  
  public init?(model pModel:Model?) {
      if let lModel = pModel {
          model = lModel
      } else {
          return nil
      }
  }
  
  public init(model pModel:Model) {
      model = pModel
  }

  // MARK: -> Public methods

  // MARK: -
  // MARK: Private access
  // MARK: -
  
  // MARK: -> Private enums
  
  // MARK: -> Private structs
  
  // MARK: -> Private class
  
  // MARK: -> Private type alias 

  // MARK: -> Private static properties

  // MARK: -> Private properties

  private var model:Model

  // MARK: -> Private static methods
  
  // MARK: -> Private init methods
  
  // MARK: -> Private operators

  // MARK: -> Private methods

  // MARK: -
  // MARK: Dummy datas
  // MARK: -
    
  //public class Dummy {
  //      
  //      public static var <#one instance#>: <#ViewModelItem#> {
  //          return <#ViewModelItem#>()
  //      }
  //}
}
```