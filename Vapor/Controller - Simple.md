# Controller - Simple

```swift
import Vapor
import Fluent
import TVaporKit

// MARK: -
// MARK: <#controller name#>Controller
// MARK: -
final public class <#controller name#>Controller: RouteCollection {
    
    // MARK: -
    // MARK: Root declaration
    // MARK: -

    public func boot(routes pRoutes: RoutesBuilder) throws {
        let l<#root#> = pRoutes.grouped("<#path1#>","<#path2#>")
        
        // URI: GET /<#path1#>/<#path2#>?<#param#>=<#value#>
        l<#root#>.get(use: get)
        
        // URI: POST /<#path1#>/<#path2#>?<#param#>=<#value#>
        l<#root#>.post(use: post)
        
        // URI: PUT /<#path1#>/<#path2#>?<#param#>=<#value#>
        l<#root#>.put(use: put)
        
        // URI: PATCH /<#path1#>/<#path2#>?<#param#>=<#value#>
        l<#root#>.put(use: patch)
        
        // URI: DELETE /<#path1#>/<#path2#>
        l<#root#>.delete(use: delete)
    }
    
    // MARK: -
    // MARK: Root methods
    // MARK: -

    private func get(on pRequest: Request) -> EventLoopFuture<Response> {
        guard let l<#param#> = pRequest.query[String.self, at: "<#param#>"] else {
            return pRequest.response(status: .badRequest)
        }
        
        // Your code here
        
        return pRequest.response(<#codable object#>)
    }
    
    private func post(on pRequest: Request) -> EventLoopFuture<Response> {
        guard let l<#param#> = pRequest.query[String.self, at: "<#param#>"] else {
            return pRequest.response(status: .badRequest)
        }
        
        // Your code here
        
        return pRequest.response(status: .created)
    }
    
    private func put(on pRequest: Request) -> EventLoopFuture<Response> {
        guard let l<#param#> = pRequest.query[String.self, at: "<#param#>"] else {
            return pRequest.response(status: .badRequest)
        }
        
        // Your code here
        
        return pRequest.response(status: .ok)
    }
    
    private func patch(on pRequest: Request) -> EventLoopFuture<Response> {
        guard let l<#param#> = pRequest.query[String.self, at: "<#param#>"] else {
            return pRequest.response(status: .badRequest)
        }
        
        // Your code here
        
        return pRequest.response(status: .ok)
    }
    
    private func delete(on pRequest: Request) -> EventLoopFuture<Response> {
        guard let l<#param#> = pRequest.query[String.self, at: "<#param#>"] else {
            return pRequest.response(status: .badRequest)
        }
        
        // Your code here
        
        return pRequest.response(status: .ok)
    }

}
```