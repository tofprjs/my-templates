# Basic - Protocol

```swift
// MARK: -
// MARK: <#protocol name#> protocol
// MARK: -
public protocol <#protocol name#> {
    // MARK: -> Static Properties
    
    // MARK: -> Properties
    
    // MARK: -> Static methods
    
    // MARK: -> Init methods
    
    // MARK: -> Public operators
    
    // MARK: -> Public methods
    
}
```
