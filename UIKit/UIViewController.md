# UIKit - UIViewController

```swift
import UIKit

// MARK: -
// MARK: <#ClassViewController#>
// MARK: -
public class  <#ClassViewController#>: UIViewController {
  // MARK: -
  // MARK: Interface
  // MARK: -
  
  // MARK: Interface Builder properties
  
  //@IBOutlet weak var view<#property#>: <#type#>!
  //@IBOutlet weak var lbl<#property#>: UILabel!
  //@IBOutlet weak var txt<#property#>: UITextView!
  //@IBOutlet weak var img<#property#>: UIAImageView!
  //@IBOutlet weak var btn<#property#>: UIButton!
  //@IBOutlet weak var barItem<#property#>: UIBarButtonItem!
  //@IBOutlet weak var bar<#property#>: UIToolbar!
  //@IBOutlet weak var webView: WKWebView!
  //@IBOutlet weak var scrollView: UIScrollView!
  //@IBOutlet weak var tableView: UITableView!
  //@IBOutlet weak var collectionView: UICollectionView!
  //@IBOutlet weak var constraint<#property#><#constraint#>: NSLayoutConstraint!

  // MARK: Interface Builder actions
  
  //@IBAction func actn<#name#>(_ pSender: Any) {
  //  // Your code here
  //}

  // MARK: Interface navigation
  
  //public override func prepare(for pSegue: UIStoryboardSegue, sender pSender: Any?) {
  //  if pSegue.identifier == "<#segue name#>" {
  //    if let l<#View Controller#> = pSegue.destination as? <#View Controller#> {
  //      // Set public properties of view controller
  //    }
  //  } else if pSegue.identifier == "<#segue name#>" {
  //    if let l<#View Controller#> = pSegue.destination as? <#View Controller#> {
  //      // Set public properties of view controller
  //    }
  //  }
  //}
  
  // MARK: Interface notifications
  
  ////@objc public func notification<#notification name#>(_ pNotification: NSNotification) {
  //  if pNotification.object is <#type#> {
  //    // <#notification code#>
  //  }
  //}
  
  // MARK: Interface class override UIViewController
  
  // Called after the controller's view is loaded into memory.
  public override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view
  }
  
  // Notifies the view controller that its view is about to be added to a view hierarchy.
  //public override func viewWillAppear(_ pAnimated: Bool) {
  //  super.viewWillAppear(pAnimated)
  //}
  
  // Notifies the view controller that its view was added to a view hierarchy.
  //public override func viewDidAppear(_ pAnimated: Bool) {
  //  super.viewDidAppear(pAnimated)
  //}
  
  // Notifies the view controller that its view is about to be removed from a view hierarchy.
  //public override func viewWillDisappear(_ pAnimated: Bool) {
  //  super.viewWillDisappear(pAnimated)
  //}
  
  // Notifies the view controller that its view was removed from a view hierarchy.
  //public override func viewDidDisappear(_ pAnimated: Bool) {
  //  super.viewDidDisappear(pAnimated)
  //}

  public override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: -
  // MARK: Public access
  // MARK: -
  
  // MARK: -> Public enums
  
  // MARK: -> Public structs
  
  // MARK: -> Public class
  
  // MARK: -> Public type alias 
  
  // MARK: -> Public static properties
  
  // MARK: -> Public properties
  
  // MARK: -> Public class methods
  
  // MARK: -> Public init methods
  
  // MARK: -> Public operators

  // MARK: -> Public methods
    
  // MARK: -
  // MARK: Internal access (aka public for current module)
  // MARK: -
  
  // MARK: -> Internal enums
  
  // MARK: -> Internal structs
  
  // MARK: -> Internal class
  
  // MARK: -> Internal type alias 
  
  // MARK: -> Internal static properties
  
  // MARK: -> Internal properties
  
  // MARK: -> Internal class methods
  
  // MARK: -> Internal operators

  // MARK: -> Internal methods

  // MARK: -
  // MARK: Private access
  // MARK: -
  
  // MARK: -> Private enums
  
  // MARK: -> Private structs
  
  // MARK: -> Private class
  
  // MARK: -> Private type alias 

  // MARK: -> Private static properties

  // MARK: -> Private properties

  // MARK: -> Private class methods
  
  // MARK: -> Private init methods
  
  // MARK: -> Private operators

  // MARK: -> Private methods
}
```