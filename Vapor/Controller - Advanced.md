# Controller - Advanced

```swift
import Vapor
import Fluent
import TVaporKit

// MARK: -
// MARK: <#controller name#>Controller
// MARK: -
final public class <#controller name#>Controller: RouteCollection {
    
    // MARK: -
    // MARK: Root declaration
    // MARK: -

    public func boot(routes pRoutes: RoutesBuilder) throws {
        // MARK: Version
        
        let lVersion = pRoutes.grouped("v1")
        
        // MARK: <#Feature1#>

        let l<#Feature1#> = lVersion.grouped("<#feature1#>")
        
        // URI: GET /v1/<#feature1#>/<#param#>
        l<#Feature1#>.get(":<#param#>", use: <#Feature1#>.get)

        // URI: POST /v1/<#feature1#>/<#param#>
        l<#Feature1#>.post(":<#param#>", use: <#Feature1#>.post)

        // URI: PUT /v1/<#feature1#>/<#param#>
        l<#Feature1#>.put(":<#param#>", use: <#Feature1#>.put)

        // URI: PATCH /v1/<#feature1#>/<#param#>
        l<#Feature1#>.patch(":<#param#>", use: <#Feature1#>.patch)

        // URI: DELETE /v1/<#feature1#>/<#param#>
        l<#Feature1#>.delete(":<#param#>", use: <#Feature1#>.delete)

        // MARK: <#Feature2#>

        let l<#Feature2#> = lVersion.grouped("<#feature2#>")
        
        // URI: GET /v1/<#feature2#>/<#param#>
        l<#Feature2#>.get(":<#param#>", use: <#Feature2#>.get)

        // URI: POST /v1/<#feature2#>/<#param#>
        l<#Feature2#>.post(":<#param#>", use: <#Feature2#>.post)

        // URI: PUT /v1/<#feature2#>/<#param#>
        l<#Feature2#>.put(":<#param#>", use: <#Feature2#>.put)

        // URI: PATCH /v1/<#feature2#>/<#param#>
        l<#Feature2#>.patch(":<#param#>", use: <#Feature2#>.patch)

        // URI: DELETE /v1/<#feature2#>/<#param#>
        l<#Feature2#>.delete(":<#param#>", use: <#Feature2#>.delete)

    }

    // MARK: -
    // MARK: <#Feature1#>
    // MARK: -

    private class <#Feature1#> {
        
        public static func get(on pRequest: Request) -> EventLoopFuture<Response> {
            guard let l<#param#> = pRequest.parameters.get("<#param#>") else {
                return pRequest.response(status: .badRequest)
            }
            
            // Your code here
            
            return pRequest.response(<#codable object#>)
        }
        
        public static func post(on pRequest: Request) -> EventLoopFuture<Response> {
            guard let l<#param#> = pRequest.parameters.get("<#param#>") else {
                return pRequest.response(status: .badRequest)
            }
            
            // Your code here
            
            return pRequest.response(status: .created)
        }
        
        public static func put(on pRequest: Request) -> EventLoopFuture<Response> {
            guard let l<#param#> = pRequest.parameters.get("<#param#>") else {
                return pRequest.response(status: .badRequest)
            }
            
            // Your code here
            
            return pRequest.response(status: .ok)
        }
        
        public static func patch(on pRequest: Request) -> EventLoopFuture<Response> {
            guard let l<#param#> = pRequest.parameters.get("<#param#>") else {
                return pRequest.response(status: .badRequest)
            }
            
            // Your code here
            
            return pRequest.response(status: .ok)
        }
        
        public static func delete(on pRequest: Request) -> EventLoopFuture<Response> {
            guard let l<#param#> = pRequest.parameters.get("<#param#>") else {
                return pRequest.response(status: .badRequest)
            }
            
            // Your code here
            
            return pRequest.response(status: .ok)
        }

    }
    
    // MARK: -
    // MARK: <#Feature2#>
    // MARK: -

    private class <#Feature2#> {
        
        public static func get(on pRequest: Request) -> EventLoopFuture<Response> {
            guard let l<#param#> = pRequest.parameters.get("<#param#>") else {
                return pRequest.response(status: .badRequest)
            }
            
            // Your code here
            
            return pRequest.response(<#codable object#>)
        }
        
        public static func post(on pRequest: Request) -> EventLoopFuture<Response> {
            guard let l<#param#> = pRequest.parameters.get("<#param#>") else {
                return pRequest.response(status: .badRequest)
            }
            
            // Your code here
            
            return pRequest.response(status: .created)
        }
        
        public static func put(on pRequest: Request) -> EventLoopFuture<Response> {
            guard let l<#param#> = pRequest.parameters.get("<#param#>") else {
                return pRequest.response(status: .badRequest)
            }
            
            // Your code here
            
            return pRequest.response(status: .ok)
        }
        
        public static func patch(on pRequest: Request) -> EventLoopFuture<Response> {
            guard let l<#param#> = pRequest.parameters.get("<#param#>") else {
                return pRequest.response(status: .badRequest)
            }
            
            // Your code here
            
            return pRequest.response(status: .ok)
        }
        
        public static func delete(on pRequest: Request) -> EventLoopFuture<Response> {
            guard let l<#param#> = pRequest.parameters.get("<#param#>") else {
                return pRequest.response(status: .badRequest)
            }
            
            // Your code here
            
            return pRequest.response(status: .ok)
        }

    }

}
```
