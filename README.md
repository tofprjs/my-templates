# My Templates

## Basic templates
- [Actor](Basic/Actor.md)
- [Class](Basic/Class.md)
- [Codable](Basic/Codable.md)
- [Extension](Basic/Extension.md)
- [Protocol](Basic/Protocol.md)

## UIKit templates
- [UIViewController](UIKit/UIViewController.md)
- [UITableViewCell](UIKit/UITableViewCell.md)
- [UITableViewDataSource](UIKit/UITableViewDataSource.md)
- [UITableViewDelegate](UIKit/UITableViewDelegate.md)

## SwiftUI templates
- [View](SwiftUI/View.md)
- [ViewModel - Item](SwiftUI/ViewModel - Item.md)
- [ViewModel - List](SwiftUI/ViewModel - List.md)
- [UIViewRepresentable](SwiftUI/UIViewRepresentable.md)

## Vapor templates
- [Controller - Simple](Vapor/Controller - Simple.md)
- [Controller - Advanced](Vapor/Controller - Advanced.md)

## Spacial templates
- [TStorage](Specials/TStorage.md)
