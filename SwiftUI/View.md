# SwiftUI - View

```swift
// MARK: -
// MARK: <#NameView#>
// MARK: -
public struct <#NameView#>: View {
    // MARK: -
    // MARK: Parameters
    // MARK: -

    // MARK: -> Parameters constantes

    //public let <#property#>: <#type#>

    // MARK: -> Parameters binding objects

    //@Binding public var <#property#>: <#type#>

    // MARK: -> Parameters closures

    //public let <#closure#>: <#closure type#>

    // MARK: -
    // MARK: Public access
    // MARK: -

    // MARK: -> Public enums

    // MARK: -> Public structs

    // MARK: -> Public class

    // MARK: -> Public type alias
  
    //public typealias <#closure type#> = <#closure declaration#>
  
    // MARK: -> Public published properties

    // MARK: -> Public static properties

    // MARK: -> Public properties

    // MARK: -> Public properties for screen
        
    // MARK: -> Public static methods
  
    // MARK: -> Public init methods
  
    // MARK: -> Public methods

    // MARK: -
    // MARK: Private access
    // MARK: -

    // MARK: -> Private environment objects

    // MARK: -> Private StateObject

    //@StateObject private var <#property#> = <#type#>()

    // MARK: -> Private ObservedObject

    //@ObservedObject private var <#property#> = <#type#>()

    // MARK: -> Private FocusState

    // MARK: -> Private State

    // MARK: -> Private Binding

    // MARK: -> Private constantes

    //private let <#property#> = <#default value#>

    // MARK: -> Private static properties

    // MARK: -> Private properties

    // MARK: -> Private static methods

    // MARK: -> Private methods

    // MARK: -
    // MARK: Body
    // MARK: -

    public var body: some View {
        VStack {
            Text(“Hello World!”)   
        }
    }
}
```
