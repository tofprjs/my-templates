# Special - TStorage

```swift
import Foundation
import TStorage
import TDataBase
import TSQLite

public enum <#EmbeddedEnum#>: <#type#>, Codable, TStorageValue {
    case <#default#>
    case <#case#>
    
    public init() {
        self = <#name#>.<#default#>
    }
}

public class <#EmbeddedObj#>: Codable, TStorageValue {
    public var <#field#>: <#type#>

    public init() {
        <#field#> = <#value#>
    }
}

// MARK: -
// MARK: <#data class#>
// MARK: -
public class <#data class#> : TStorage {
    // MARK: -
    // MARK: Public TStorage
    // MARK: -
    
    // MARK: -> Table and Database

    public static var table:String = "<#data class#>"
    public static var dataBase:TDataBase = TSqlite.appInstance()

    // MARK: -> Properties

    public var properties: TProperties

    // MARK: -> Fields

    @Field(key: "id", options: [.primary(.asc), .autoincrement])
    public var id: Int

    @Field(key: "<#field#>", options: [.index])
    public var <#field#>: <#type#>

    @Field(key: "<#field#>", options: [.index, .unique])
    public var <#field#>: <#type#>

    @ToOne(key: "<#foreignKey#>", table: <#TStorage class#>.self, field: "<#TStorage.field#>")
    public var <#field#>:<#TStorage class#>?

    @ToMany(key: "<#foreignKey#>", table: <#TStorage class#>.self, field: "<#TStorage.field#>")
    public var <#field#>:[<#TStorage class#>]?

    @Fetched(table: <#TStorage class#>.self, sql: .where(exp:<#expression#>))
    public var <#field#>:[<#TStorage class#>]?

    @Json(key: "<#field#>")
    public var <#field#>:<#EmbeddedEnum#>?

    @Json(key: "<#field#>")
    public var <#field#>:[<#EmbeddedObj#>]?

    @Field(key: "<#field#>", default: <#value#>)
    public var <#field#>: <#type#>

    @Field(key: "<#field#>")
    public var <#field#>: <#type#>

    @Field(key: "<#field#>")
    public var <#field#>: <#type#>?

    // MARK: -> Init

    required public init() {
        properties = TProperties()
        setup()
    }

    // MARK: -
    // MARK: Public access
    // MARK: -
    
    // MARK: -> Public enums
    
    // MARK: -> Public structs
    
    // MARK: -> Public class
    
    // MARK: -> Public type alias
    
    // MARK: -> Public static properties
    
    // MARK: -> Public properties
    
    public var <#property#>:<#type#> = <#value#>
    
    // MARK: -> Public static methods
    
    // MARK: -> Public init methods
    
    // MARK: -> Public methods

    // MARK: -
    // MARK: Internal access (aka public for current module)
    // MARK: -
    
    // MARK: -> Internal enums
    
    // MARK: -> Internal structs
    
    // MARK: -> Internal class
    
    // MARK: -> Internal type alias 
    
    // MARK: -> Internal static properties
    
    // MARK: -> Internal properties
  
    // MARK: -> Internal static methods
    
    // MARK: -> Internal operators
  
    // MARK: -> Internal methods
    
    // MARK: -
    // MARK: Private access
    // MARK: -
    
    // MARK: -> Private enums
    
    // MARK: -> Private structs
    
    // MARK: -> Private class
    
    // MARK: -> Private type alias 
  
    // MARK: -> Private static properties
  
    // MARK: -> Private properties
  
    // MARK: -> Private static methods
    
    // MARK: -> Private init methods
    
    // MARK: -> Private operators
  
    // MARK: -> Private methods  
}

extension <#data class#> {
    
    public class Dummy {
        public static var empty: [<#data class#>] {
            return []
        }
        
        public static var item: <#data class#> {
            let l<#data class#> = <#data class#>()
            l<#Data class#>.<#field#> = <#value#>
            return l<#Data class#>
        }
        
        public static var items: [<#data class#>] {
            let l<#Data class#>s = (1...10).map({
                pI -> <#data class#> in
                let l<#Data class#> = <#data class#>()
                l<#Data class#>.<#field#> = <#value#>
                return l<#Data class#>
            })
            
            return l<#Data class#>s
        }
        
    }
}
```