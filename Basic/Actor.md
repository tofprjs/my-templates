# Basic - Actor

```swift
// MARK: -
// MARK: <#actor#>
// MARK: -
public actor <#actor#> {
  // MARK: -
  // MARK: Public access
  // MARK: -
  
  // MARK: -> Public enums
  
  // MARK: -> Public structs
  
  // MARK: -> Public classes
  
  // MARK: -> Public actors
  
  // MARK: -> Public type alias
  
  // MARK: -> Public static properties
  
  // MARK: -> Public published properties
  
  // MARK: -> Public binding properties
  
  // MARK: -> Public properties
  
  // MARK: -> Public static methods
  
  // MARK: -> Public init methods
  
  // MARK: -> Public operators
  
  // MARK: -> Public methods
  
  // MARK: -
  // MARK: Internal access (aka public for current module)
  // MARK: -
  
  // MARK: -> Internal enums
  
  // MARK: -> Internal structs
  
  // MARK: -> Internal classes
  
  // MARK: -> Internal actors
  
  // MARK: -> Internal type alias
  
  // MARK: -> Internal static properties
  
  // MARK: -> Internal published properties
  
  // MARK: -> Internal binding properties
  
  // MARK: -> Internal properties
  
  // MARK: -> Internal static methods
  
  // MARK: -> Internal operators
  
  // MARK: -> Internal methods
  
  // MARK: -
  // MARK: Private access
  // MARK: -
  
  // MARK: -> Private enums
  
  // MARK: -> Private structs
  
  // MARK: -> Private classes
  
  // MARK: -> Private actor
  
  // MARK: -> Private type alias
  
  // MARK: -> Private static properties
  
  // MARK: -> Private published properties
  
  // MARK: -> Private binding properties
  
  // MARK: -> Private properties
  
  // MARK: -> Private static methods
  
  // MARK: -> Private init methods
  
  // MARK: -> Private operators
  
  // MARK: -> Private methods
}
```