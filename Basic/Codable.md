# Base - Codable

```swift
// MARK: -
// MARK: <#class#>
// MARK: -
public class <#class#>: Codable {
    // MARK: -
    // MARK: Public access
    // MARK: -
    
    // MARK: -> Public enums
    
    public enum CodingKeys: String, CodingKey {
        case <#property#>
    }
    
    // MARK: -> Public structs
    
    // MARK: -> Public class
    
    // MARK: -> Public type alias
    
    // MARK: -> Public static properties
    
    // MARK: -> Public properties
    
    public var <#property#>:<#type#> = <#default value#>
    
    // MARK: -> Public static methods
    
    // MARK: -> Public init methods
    
    public init() {}
    
    // MARK: -> Public methods
    
    // MARK: -> Public protocol Encodable
    
    public func encode(to pEncoder: Encoder) throws {
        var lContainer = pEncoder.container(keyedBy: CodingKeys.self)
        
        try lContainer.encode(<#property#>, forKey: .<#property#>)
    }
    
    // MARK: -> Public protocol Decodable
    
    public required init(from pDecoder: Decoder) throws {
        if let lContainer = try? pDecoder.container(keyedBy: CodingKeys.self) {
            var lInvalidFields:[String] = []
            
            // Required
            if let l<#property#> = try? lContainer.decode(String.self, forKey: .<#property#>) {
                id = l<#property#>
            } else {
                lInvalidFields.append(CodingKeys.<#property#>.stringValue)
            }
            
            if lInvalidFields.isEmpty == false {
                print("⚠️ \(Swift.type(of: self)): invalid fields: [\(lInvalidFields.joined(separator: ","))]")
            }
        } else {
            print("⚠️ \(type(of: self)): invalid json")
        }
    }

    // MARK: -
    // MARK: Internal access (aka public for current module)
    // MARK: -
    
    // MARK: -> Internal enums
    
    // MARK: -> Internal structs
    
    // MARK: -> Internal class
    
    // MARK: -> Internal type alias
    
    // MARK: -> Internal static properties
    
    // MARK: -> Internal properties
    
    // MARK: -> Internal static methods
    
    // MARK: -> Internal init methods
    
    // MARK: -> Internal operators
    
    // MARK: -> Internal methods
    
    // MARK: -
    // MARK: Private access
    // MARK: -
    
    // MARK: -> Private enums
    
    // MARK: -> Private structs
    
    // MARK: -> Private class
    
    // MARK: -> Private type alias
    
    // MARK: -> Private static properties
    
    // MARK: -> Private properties
    
    // MARK: -> Private static methods
    
    // MARK: -> Private init methods
    
    // MARK: -> Private operators
    
    // MARK: -> Private methods
    
}
```