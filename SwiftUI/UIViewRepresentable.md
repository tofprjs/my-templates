# UIViewRepresentable

```swift
// MARK: -
// MARK: <#NameVieww#>
// MARK: -
public struct <#NameView#>: UIViewRepresentable {
    // MARK: -
    // MARK: Parameters
    // MARK: -
    
    // MARK: -> Parameters constantes
    
    //public let <#property#>: <#type#>
    
    // MARK: -> Parameters binding objects
    
    //@Binding public var <#property#>: <#type#>
    
    // MARK: -> Parameters closures
    
    //public let <#closure#>: <#closure type#>

    // MARK: -
    // MARK: Public access
    // MARK: -
    
    // MARK: -> Public enums
    
    // MARK: -> Public structs
    
    // MARK: -> Public class

    // MARK: -> Public type alias
    
    //public typealias <#closure type#> = <#closure declaration#>
    
    // MARK: -> Public published properties
    
    // MARK: -> Public static properties
    
    // MARK: -> Public properties
    
    // MARK: -> Public static methods
    
    // MARK: -> Public init methods
    
    // MARK: -> Public methods
        
    // Create view
    public func makeUIView(context pContext: Context) -> <#UIKit type#> {
        let l<#UIKit type#> = <#UIKit type#>()

        // Settings of <#UIKit type#>
        //...

        // Coordinator for delegate
        l<#UIKit type#>.delegate = pContext.coordinator

        return l<#UIKit type#>
    }

    // Update view
    public func updateUIView(_ p<#UIKit type#>: <#UIKit type#>, context pContext: Context) {
        // Settings of <#UIKit type#>
    }

    // Coordinator to encapsulate Delegate
    public func makeCoordinator() -> Coordinator {
        Coordinator(<#parameter#>)
    }
}

extension <#NameView#> {

    // MARK: -
    // MARK: <#NameVieww#>.Coordinator
    // MARK: -
    public final class Coordinator: NSObject, <#UIKit type#>Delegate {
        // MARK: -
        // MARK: Parameters
        // MARK: -
    
        // MARK: -> Parameters constantes
        
        //public let <#property#>: <#type#>
        
        // MARK: -> Parameters binding objects
        
        //@Binding public var <#property#>: <#type#>
        
        // MARK: -> Parameters closures
        
        //public let <#closure#>: <#closure type#>

        // MARK: -
        // MARK: Public access
        // MARK: -
    
        // MARK: -> Public init methods

        public init(_ p<#parameter#>: Binding<<#type#>>) {
            <#parameter#> = p<#parameter#>
        }

        // MARK: -> Public deletegate's methods
        //...

    }

}
```
